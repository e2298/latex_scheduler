INPUT = input
INPUT_START = 1
EXEC = scheduler
OUTPUT = output#name of output file, without extension
MAXPROC = 100 #max number of processes
MAXDUR = 100 #max duration of a single process
CFILES = main.c schedulers.c latexer.c priority_queue.c
CFILES := $(addprefix src/,$(CFILES))
HEADERS = schedulers.h latexer.h priority_queue.h
HEADERS := $(addprefix src/,$(HEADERS))
CC = gcc -std=c11
OPTIONS = -o $(EXEC)

all: $(OUTPUT).pdf clean_trash

$(EXEC): $(CFILES) $(HEADERS)
	$(CC) $(CFILES) $(OPTIONS) -DMAXP=$(MAXPROC) -DMAXD=$(MAXDUR) -DOFILE=\"$(OUTPUT)\"

$(OUTPUT).tex: $(EXEC) flatex $(INPUT)
	tail -n +$(INPUT_START) $(INPUT) | ./$(EXEC)
	./flatex -q $(OUTPUT).tex
	mv $(OUTPUT).flt $(OUTPUT).tex

$(OUTPUT).pdf: $(OUTPUT).tex
	pdflatex -interaction=nonstopmode $(OUTPUT).tex

flatex: src/flatex.c
	$(CC) src/flatex.c -o flatex

.PHONY: clean clean_exe clean_trash

#clean all executables
clean_exe:
	rm -f $(EXEC) flatex

#clean all intermediate files exept executables
clean_trash:
	rm -f $(OUTPUT).aux $(OUTPUT).log $(OUTPUT).tex

#clean all created files exept the output pdf
clean: clean_exe clean_trash

