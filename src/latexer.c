#include"latexer.h"
#include"schedulers.h"
#include<stdio.h>
#include<stdlib.h>

FILE* file;

void init_latex(char* fname){
	file = fopen(fname, "w+");
	fprintf(file, "\\input{aux/latexbegin.tex}\n");
}

void latex_section(char* s){
	fprintf(file, "\\section{%s}\n", s);
}

void schedule2latex(){
	int rsize = 0;
	
	int** stimes = malloc(MAXP * sizeof(int*));
	int** etimes = malloc(MAXP * sizeof(int*));
	int* ridx = calloc(MAXP, sizeof(int));//indices to stimes and rtimes

	for(int i = 0; i < MAXP; i++){
		stimes[i] = malloc(MAXD * sizeof(int));
		etimes[i] = malloc(MAXD * sizeof(int));
	}

	for(int i = 0; i < RSIZE; i++){
		int curr = RIDX[i];
		stimes[curr][ ridx[curr]] = STIME[i];
		etimes[curr][ ridx[curr]] = RTIME[i] + STIME[i] - 1;
		ridx[curr]++;

		if(STIME[i] + RTIME[i] > rsize)
			rsize = STIME[i] + RTIME[i];
	}

	fprintf(file, "\\input{aux/ganttbegin.tex}");
	fprintf(file, "{%d}\n", rsize - 1);
	fprintf(file, "\\gantttitlelist{0,...,%d}{1}", rsize - 1);
	
	for(int i = 0; i < PROCESSES; i++){
		if(ridx[i] > 0){
			fprintf(file, "\\\\\n\\ganttbar{%d}{%d}{%d} ",
			  ID[i], stimes[i][0], etimes[i][0]);
		}
		for(int j = 1; j < ridx[i]; j++){
			fprintf(file, "\\ganttbar{}{%d}{%d} ",
			  stimes[i][j], etimes[i][j]);
		}
	}
	fprintf(file, "\\end{ganttchart}\n");
}

void end_latex(){
	fprintf(file, "\\end{document}");
	fclose(file);
}
