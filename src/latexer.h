#ifndef LATEXER
#define LATEXER
#include<stdio.h>

//open the file fname, and write some latex stuff to it
void init_latex(char* fname);

//create a latex section with the given name in the current file
void latex_section(char* s);

//convert the schedule described by RIDX, STIME and RTIME to latex, and write it to the current file
void schedule2latex();

//close current file and do latex stuff
void end_latex();

#endif
