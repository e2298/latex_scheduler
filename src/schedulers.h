#ifndef SCHEDULERS
#define SCHEDULERS

//max processes
#ifndef MAXP
#define MAXP 200
#endif

//max process duration
#ifndef MAXD
#define MAXD 100
#endif

int PROCESSES; //number of processes
int ID[MAXP]; //IDs of processes
int ATIME[MAXP]; //Arrival times
int DUR[MAXP]; //Durations
int PRT[MAXP]; //Priorities

//results
int RSIZE; //number of process runs
int RIDX[MAXP*MAXD]; //index of running process
int STIME[MAXP*MAXD]; //at what time each process starts
int RTIME[MAXP*MAXD]; //for how long each process runs

//run after filling ID,ATIME,DUR, and PRT, before running any scheduler
void init_schedulers();

//run the scheduler on the processes described above, and put the results on RSIZE, RIDX, STIME and RTIME
void fifo();
void stf();
void srt();
void rr(int quantum);
void hpf();
void hpf_ex();

#endif
