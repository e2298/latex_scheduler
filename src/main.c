#include<stdio.h>
#include<stdlib.h>
#include"schedulers.h"
#include"latexer.h"

#ifndef OFILE
#define OFILE "out"
#endif

//writes current scheduler results to latex
void schedule2latex();

int main(void){
	int id, atime, dur, prt;
	while(scanf("%d %d %d %d", &id, &atime, &dur, &prt) != EOF){
		ID[PROCESSES] = id;
		ATIME[PROCESSES] = atime;
		DUR[PROCESSES] = dur;
		PRT[PROCESSES] = prt;

		PROCESSES++;
	}
	
	init_schedulers();
	//init_latex("out.tex");
	init_latex(OFILE".tex");

	fifo();
	latex_section("FIFO");
	schedule2latex();
	rr(2);
	latex_section("Round robin (2)");
	schedule2latex();
	rr(3);
	latex_section("Round robin (3)");
	schedule2latex();
	stf();
	latex_section("STF");
	schedule2latex();
	srt();
	latex_section("SRT");
	schedule2latex();
	hpf();
	latex_section("HPF");
	schedule2latex();
	hpf_ex();	
	latex_section("HPF-EXP");
	schedule2latex();

	end_latex();

	return 0;
}

