#include"schedulers.h"
#include"priority_queue.h"
#include<stdlib.h>
#include<string.h>

int SORTED[MAXP]; //indeces of PROCESSES, sorted  by arrival time
//remaining time
int rt[MAXP]; 
//insertion time (for use in round robin)
int it[MAXP];

//commare ATIME[*i] to ATIME[*j] (arguments are ints)
int compare_atime(const void* i, const void* j){
	int ii = *(int*)i;
	int jj = *(int*)j;
	if(ATIME[ii] == ATIME[jj]) return ID[ii] - ID[jj];
	else return ATIME[ii] - ATIME[jj];
}

//sort processes according to their arrival time
void init_schedulers(){
	int n = PROCESSES;
	for(int i = 0; i < n; i++) SORTED[i] = i;

	qsort(SORTED, n, sizeof(int), compare_atime);
}

void fifo(){
	int n = PROCESSES;
	RSIZE = n;

	int ctime = 0; //current time;
	int *process = SORTED; //idx of current process
	for(int i = 0; i < n; i++, process++){
		/* just execute processes in the order they arrive*/
		
		//current procees comes later that last one's end
		if(ATIME[*process] > ctime){
			ctime = ATIME[*process];	
		}
		RIDX[i] = *process;
		RTIME[i] = DUR[*process];
		STIME[i] = ctime;
		ctime += DUR[*process];
	}
}

int compare_itime(int i, int j){
	if(it[i] == it[j]) return ID[i] - ID[j];
	else return it[i] - it[j];
}

void rr(int quantum){
	int n = PROCESSES;
	priority_queue pq = pq_init(n, compare_itime); 
	for(int i =0; i < n; i++){
		it[i] = ATIME[i];
		rt[i] = DUR[i];
		pq_queue(&pq, i);
	}

	int ctime = 0; 
	int ri = 0;
	do{	
		int curr = pq_pop(&pq);

		if(it[curr] > ctime) ctime = it[curr];//jump over void in processes

		RIDX[ri] = curr;
		STIME[ri] = ctime;
	
		int rtime; //for how long the current process will run, this is either its remaining time, or quantum

		if(rt[curr] <= quantum){
			rtime = rt[curr];
		}
		else {
			rtime = quantum;
			rt[curr] -= quantum;
			it[curr] = ctime + quantum;
			pq_queue(&pq, curr);
		}

		ctime += rtime;
		RTIME[ri] = rtime;

		ri++;
	}while(!pq_empty(&pq));

	RSIZE = ri;

	pq_del(&pq);
}

int compare_dur(int i, int j){
	if(DUR[i] == DUR[j]) return ID[i] - ID[j];
	else return DUR[i] - DUR[j];
}

void stf(){
	int n = PROCESSES;
	RSIZE = n;

	int ctime = 0;
	int*process = SORTED;
	priority_queue pq = pq_init(n, compare_dur);
	for(int i = 0; i < n; i++){
		if(process < SORTED+n && ATIME[*process] > ctime){ //fill void in processes
			ctime = ATIME[*process];
		}
		while(process < SORTED+n && ATIME[*process] <= ctime){ //get all entered processed into queue
			pq_queue(&pq, *process);
			process++;
		}	

		int next = pq_pop(&pq);

		RIDX[i] = next;
		STIME[i] = ctime;
		RTIME[i] = DUR[next];
		ctime += DUR[next];
	}

	pq_del(&pq);
}

int compare_remaining(int i, int j){
	if(rt[i] == rt[j]) return ID[i] - ID[j];
	else return rt[i] - rt[j];
}

void srt(){
	int n = PROCESSES;

	memcpy(rt, DUR, n*sizeof(int));

	priority_queue pq = pq_init(n, compare_remaining);
	int* process = SORTED;

	int ri = -1;
	int curr = *process;
	int last = -1;
	RIDX[ri] = *process;
	int ctime = ATIME[*process];
	process++;

	do{
		while(process < SORTED+n && ATIME[*process] <= ctime){
			pq_queue(&pq, *process);
			process++;
		}	
		if(rt[curr] == 0){
			curr = pq_pop(&pq);
		}
		else if(compare_remaining(pq.q[0], curr) < 0){ //top of queue has lower remaining time than the current process
			pq_queue(&pq, curr);
			curr = pq_pop(&pq);
		}

		if(last != curr){ //process was swapped
			ri++;
			RIDX[ri] = curr;
			STIME[ri] = ctime;
			RTIME[ri] = 0;	
		}

		//run until new process enters or current finishes
		if(process < SORTED+n && (ATIME[*process] - ctime) < rt[curr]){ 
			int diff = ATIME[*process] - ctime;
			RTIME[ri] += diff;
			ctime += diff;
			rt[curr] -= diff;
		}
		else{
			RTIME[ri] += rt[curr];
			ctime += rt[curr];
			rt[curr] = 0;
		}
		
		last = curr;
		if(!pq_empty(&pq) && process < SORTED+n){
			ctime = ATIME[*process];
		}
	}while(!pq_empty(&pq) || process< SORTED+n);

	RSIZE = ri+1;
	pq_del(&pq);
}
	


//hpf and hpf-exp are pretty much just spf and srt with different comparison funtions


int compare_priority(int i, int j){
	if(PRT[i] == PRT[j]) return ID[i] - ID[j];
	else return PRT[i] - PRT[j];
}

void hpf(){
	int n = PROCESSES;
	RSIZE = n;

	int ctime = 0;
	int*process = SORTED;
	priority_queue pq = pq_init(n, compare_priority);
	for(int i = 0; i < n; i++){
		if(process < SORTED+n && ATIME[*process] > ctime){
			ctime = ATIME[*process];
		}
		while(process < SORTED+n && ATIME[*process] <= ctime){
			pq_queue(&pq, *process);
			process++;
		}	

		int next = pq_pop(&pq);

		RIDX[i] = next;
		STIME[i] = ctime;
		RTIME[i] = DUR[next];
		ctime += DUR[next];
	}

	pq_del(&pq);
}

void hpf_ex(){
	int n = PROCESSES;

	memcpy(rt, DUR, n*sizeof(int));

	priority_queue pq = pq_init(n, compare_priority);
	int* process = SORTED;

	int ri = -1;
	int curr = *process;
	int last = -1;
	RIDX[ri] = *process;
	int ctime = ATIME[*process];
	process++;

	do{
		while(process < SORTED+n && ATIME[*process] <= ctime){
			pq_queue(&pq, *process);
			process++;
		}	
		if(rt[curr] == 0){
			curr = pq_pop(&pq);
		}
		else if(compare_priority(pq.q[0], curr) < 0){
			pq_queue(&pq, curr);
			curr = pq_pop(&pq);
		}

		if(last != curr){
			ri++;
			RIDX[ri] = curr;
			STIME[ri] = ctime;
			RTIME[ri] = 0;	
		}

		if(process < SORTED+n && (ATIME[*process] - ctime) < rt[curr]){
			int diff = ATIME[*process] - ctime;
			RTIME[ri] += diff;
			ctime += diff;
			rt[curr] -= diff;
		}
		else{
			RTIME[ri] += rt[curr];
			ctime += rt[curr];
			rt[curr] = 0;
		}
		
		last = curr;
		if(!pq_empty(&pq) && process < SORTED+n){
			ctime = ATIME[*process];
		}
	}while(!pq_empty(&pq) || process< SORTED+n);

	RSIZE = ri+1;
	pq_del(&pq);
}
