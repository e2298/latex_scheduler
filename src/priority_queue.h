#ifndef PQ
#define PQ
#include<stdlib.h>

//min priority queue

typedef struct{
	int size;
	int* q;
	int (*comp)(int, int);
} priority_queue;

priority_queue pq_init(int size, int(*comp)(int, int)); //get a priority queue with max capacity size, sorted by comp

void pq_del (priority_queue* pq); //remove priority queue

void pq_queue(priority_queue* pq, int val); //insert val into pq

int pq_pop(priority_queue* pq); //remove first element and return it

int pq_top(priority_queue* pq); //peek at the top

int pq_empty(priority_queue* pq); //check if pq is empty

#endif
