#include"priority_queue.h"
#include<stdio.h>

priority_queue pq_init(int size, int (*comp)(int,int)){
	priority_queue new;
	new.size = 0;
	new.q = malloc(size*sizeof(int));
	new.comp = comp;
	return new;
}

void pq_del(priority_queue* pq){
	free(pq-> q);
}

void pq_queue(priority_queue* pq, int val){
	int* q = pq->q;
	q[pq->size] = val;
	int i = pq->size;
	while(i){
		int p = (i-1)/2;
		if(pq->comp(q[i], q[p]) < 0){
			q[i] ^= q[p];
			q[p] ^= q[i];
			q[i] ^= q[p];
			i = p;
		}
		else break;
	}

	pq->size++;		
}

int pq_pop(priority_queue* pq){
	int* q = pq->q;
	int top = q[0];
	pq->size--;
	q[0] = q[pq->size];

	int curr = 0;
	while(curr < pq->size){
		int l = 2*curr + 1;
		int r = 2*(curr + 1);
		
		int nexti;

		if(l >= pq->size) break;
		else if( r >= pq->size){
			nexti = l;	
		}
		else{
			if(pq->comp(q[r], q[l]) < 0) nexti = r;
			else nexti = l;
		}

		if(pq->comp(q[curr], q[nexti]) > 0){
			q[curr] ^= q[nexti];
			q[nexti] ^= q[curr];
			q[curr] ^= q[nexti];
			curr = nexti;
		}
		else break;
	}
	return top;
}
int pq_top(priority_queue* pq){
	return pq->q[0];
}

int pq_empty(priority_queue* pq){
	return !pq->size;
}
